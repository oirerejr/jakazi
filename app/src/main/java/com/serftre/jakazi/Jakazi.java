package com.serftre.jakazi;

import android.app.Application;

/**
 * Created by oirere on 04/12/2018.
 */

public class Jakazi extends Application {

    private static Jakazi jakazi;

    @Override
    public void onCreate() {
        super.onCreate();
        Jakazi.jakazi = this;
    }

    public static Jakazi get() {
        return jakazi;
    }

}
