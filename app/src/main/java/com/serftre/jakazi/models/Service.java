package com.serftre.jakazi.models;

/**
 * Created by oirere on 29/11/2018.
 */

public class Service {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
