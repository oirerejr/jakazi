package com.serftre.jakazi.models;

/**
 * Created by oirere on 30/12/2018.
 */

public class Page {

    private static Page page;
    String title, content;
    int image;

    public Page(String title, String content, int image) {
        this.title = title;
        this.content = content;
        this.image = image;
    }

    public static Page get(String title, String content, int image) {
        page = new Page(title, content, image);
        return page;
    }

}