package com.serftre.jakazi.models;

/**
 * Created by oirere on 04/12/2018.
 */

public class Item {

    private String id, name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
