package com.serftre.jakazi.models;

import java.util.ArrayList;

/**
 * Created by oirere on 29/11/2018.
 */

public class Maid {

    private String name, id, phone;
    private double rating;
    private float serviceRate;
    private boolean status;
    private ArrayList<Service> services;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public float getServiceRate() {
        return serviceRate;
    }

    public void setServiceRate(float serviceRate) {
        this.serviceRate = serviceRate;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    private String getIdNumber() {
        return id;
    }

    private void setIdNumber(String idNumber) {
        this.id = idNumber;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public ArrayList<Service> getServices() {
        return services;
    }

    public void setServices(ArrayList<Service> services) {
        this.services = services;
    }
}
