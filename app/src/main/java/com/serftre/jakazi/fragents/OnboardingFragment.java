package com.serftre.jakazi.fragents;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.serftre.jakazi.Jakazi;
import com.serftre.jakazi.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by oirere on 30/12/2018.
 */

public class OnboardingFragment extends Fragment {

    private static String IMAGE = "image";
    private static String TITLE = "title";
    private static String CONTENT = "content";

    @BindView(R.id.image)
    ImageView imageView;

    @BindView(R.id.title)
    TextView title;

    @BindView(R.id.content)
    TextView content;

    public static OnboardingFragment newInstance(int image, String title, String content) {
        OnboardingFragment onboardingFragment = new OnboardingFragment();
        Bundle args = new Bundle();
        args.putInt(IMAGE, image);
        args.putString(TITLE, title);
        args.putString(CONTENT, content);
        onboardingFragment.setArguments(args);
        return onboardingFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.onboarding_layout, container, false);
        ButterKnife.bind(this, view);

        Bundle bundle = getArguments();

        int image = bundle.getInt(IMAGE);
        String title = bundle.getString(TITLE);
        String content = bundle.getString(CONTENT);

        Drawable drawable = ContextCompat.getDrawable(Jakazi.get(), image);

        OnboardingFragment.this.title.setText(title);
        OnboardingFragment.this.content.setText(content);
        OnboardingFragment.this.imageView.setImageDrawable(drawable);

        return view;
    }
}
