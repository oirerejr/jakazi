package com.serftre.jakazi.adapters;

import android.Manifest;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.serftre.jakazi.R;
import com.serftre.jakazi.models.Maid;
import com.serftre.jakazi.models.Service;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by oirere on 29/11/2018.
 */

public class ToppickAdapter extends RecyclerView.Adapter<ToppickAdapter.ViewHolder> {

    public interface MaidClickListener {
        void maidClicked(Maid maid);
    }

//    private MaidClickListener maidClickListener;

    private Context context;
    private ArrayList<Maid> maids;

    public ToppickAdapter(Context context, ArrayList<Maid> maids) {
        this.context = context;
        this.maids = maids;
//        maidClickListener = (MaidClickListener) context;
    }

    @NonNull
    @Override
    public ToppickAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.top_pick_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ToppickAdapter.ViewHolder holder, int i) {
        Maid maid = maids.get(i);
        holder.viewIndicator.setBackgroundColor(context.getResources().getColor(i % 2 == 0 ? R.color
                .colorRedAccent
                : R.color
                .colorGreenAccent));
    }

    @Override
    public int getItemCount() {
        return maids.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.view_indicator)
        View viewIndicator;
//
//        @BindView(R.id.services)
//        TextView services;
//
//        @BindView(R.id.rating)
//        TextView rating;
//
//        @BindView(R.id.status)
//        TextView status;
//
//        @BindView(R.id.service_rate)
//        TextView serviceRate;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
//
//        @OnClick(R.id.view)
//        public void clicked() {
//            maidClickListener.maidClicked(maids.get(getAdapterPosition()));
//        }

    }
}
