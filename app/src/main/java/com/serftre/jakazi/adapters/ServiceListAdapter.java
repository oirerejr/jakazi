package com.serftre.jakazi.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.serftre.jakazi.R;
import com.serftre.jakazi.models.Item;
import com.serftre.jakazi.models.Service;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by oirere on 29/11/2018.
 */

public class ServiceListAdapter extends RecyclerView.Adapter<ServiceListAdapter.ViewHolder> {

    private int lastSelectedIndex = 0;

    public interface ServiceListInterface {
        void serviceClicked(Item service);
    }

    private ServiceListInterface serviceListInterface;

    private Context context;
    private ArrayList<Item> services;

    public ServiceListAdapter(Context context, ArrayList<Item> services) {
        this.context = context;
        this.services = services;
        serviceListInterface = (ServiceListInterface) context;
    }

    @NonNull
    @Override
    public ServiceListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_service_layout, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceListAdapter.ViewHolder holder, int i) {
        Item service = services.get(i);
        holder.serviceName.setText(service.getName());

        Drawable black = ContextCompat.getDrawable(context, R.drawable.item_service_background_black);
        Drawable gray = ContextCompat.getDrawable(context, R.drawable.item_service_background_gray);

        if (lastSelectedIndex == i) {
            holder.serviceName.setBackground(black);
        } else {
            holder.serviceName.setBackground(gray);
        }

    }

    @Override
    public int getItemCount() {
        return services.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.service_name)
        TextView serviceName;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            serviceName.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            serviceListInterface.serviceClicked(services.get(getAdapterPosition()));
            lastSelectedIndex = getAdapterPosition();
            notifyDataSetChanged();
        }

    }
}
