package com.serftre.jakazi.adapters;

import android.Manifest;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.serftre.jakazi.R;
import com.serftre.jakazi.models.Maid;
import com.serftre.jakazi.models.Service;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by oirere on 29/11/2018.
 */

public class MaidAdapter extends RecyclerView.Adapter<MaidAdapter.ViewHolder> {

    public interface MaidClickListener {
        void maidClicked(Maid maid);
    }

    private MaidClickListener maidClickListener;

    private Context context;
    private ArrayList<Maid> maids;

    public MaidAdapter(Context context, ArrayList<Maid> maids) {
        this.context = context;
        this.maids = maids;
        maidClickListener = (MaidClickListener) context;
    }

    @NonNull
    @Override
    public MaidAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.top_pick_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MaidAdapter.ViewHolder holder, int i) {
        Maid maid = maids.get(i);
//        holder.name.setText(maid.getName());
//
//        if (maid.getServices() != null) {
//            StringBuilder servs = new StringBuilder();
//            for (Service serv :
//                    maid.getServices()) {
//                servs.append(serv.getName()).append(", ");
//            }
//
//            holder.services.setText(servs.toString().substring(0, servs.toString().trim().length() - 1));
//        } else {
//            holder.services.setText("General House Cleaning");
//        }
//
//        if (maid.getRating() <= 0) {
//            holder.rating.setText(String.valueOf(5.0));
//            holder.rating.setText(String.valueOf(5.0));
//        } else {
//            holder.rating.setText(String.valueOf(maid.getRating()));
//        }
//
//        Drawable busy = ContextCompat.getDrawable(context, R.drawable.item_maid_busy_background);
//        Drawable idle = ContextCompat.getDrawable(context, R.drawable.item_maid_idle_background);
//
//        if (maid.isStatus()) {
//            holder.status.setText("available");
//            holder.status.setBackground(idle);
//        } else {
//            holder.status.setText("busy");
//            holder.status.setBackground(busy);
//        }
//
//        if (maid.getServiceRate() <= 0) {
//            holder.serviceRate.setText(String.format(context.getResources().getString(R.string.starts_from), "N/A"));
//        } else {
//            holder.serviceRate.setText(String.format(context.getResources().getString(R.string.starts_from), String.valueOf(maid.getServiceRate())));
//        }

    }

    @Override
    public int getItemCount() {
        return maids.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

//        @BindView(R.id.name)
//        TextView name;
//
//        @BindView(R.id.services)
//        TextView services;
//
//        @BindView(R.id.rating)
//        TextView rating;
//
//        @BindView(R.id.status)
//        TextView status;
//
//        @BindView(R.id.service_rate)
//        TextView serviceRate;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.view)
        public void clicked() {
            maidClickListener.maidClicked(maids.get(getAdapterPosition()));
        }

    }
}
