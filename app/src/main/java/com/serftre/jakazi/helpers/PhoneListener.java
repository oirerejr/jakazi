package com.serftre.jakazi.helpers;

import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.widget.Toast;

/**
 * Created by oirere on 30/11/2018.
 */

public class PhoneListener extends PhoneStateListener {

    public interface PhoneListenerInterface {
        void ringing(int state, String phoneNumber);

        void offHook(int state, String phoneNumber);

        void idle(int state, String phoneNumber);
    }

    private PhoneListenerInterface phoneListenerInterface;

    public void setListener(PhoneListenerInterface phoneListenerInterface) {
        this.phoneListenerInterface = phoneListenerInterface;
    }

    @Override
    public void onCallStateChanged(int state, String phoneNumber) {
        super.onCallStateChanged(state, phoneNumber);
        if (state == TelephonyManager.CALL_STATE_RINGING) {
            phoneListenerInterface.ringing(state, phoneNumber);
        }
        if (state == TelephonyManager.CALL_STATE_OFFHOOK) {
            phoneListenerInterface.offHook(state, phoneNumber);
        }
        if (state == TelephonyManager.CALL_STATE_IDLE) {
            phoneListenerInterface.idle(state, phoneNumber);
        }
    }
}
