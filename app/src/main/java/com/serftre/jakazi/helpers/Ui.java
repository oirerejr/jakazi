package com.serftre.jakazi.helpers;

import android.view.View;
import android.view.Window;

public class Ui {

    private static Ui instance;
    private static final Object mutex = new Object();

    public static Ui getInstance() {
        Ui ui = instance;
        if (ui == null) {
            synchronized (mutex) {
                ui = instance;
                if (ui == null)
                    instance = ui = new Ui();
            }
        }
        return instance;
    }

    public void hideSystemUI(Window window) {
        View decorView = window.getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

}
