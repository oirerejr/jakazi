package com.serftre.jakazi.helpers;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.serftre.jakazi.Jakazi;
import com.serftre.jakazi.models.User;

/**
 * Created by oirere on 30/11/2018.
 */

public class Session {

    private static Session session;

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    public static final String WORK_IN_PROGRESS = "workinprogress";
    public static final String PREFS = "jakaziprefs";
    public static final String USER = "user";

    private Session() {
        sharedPreferences = Jakazi.get().getSharedPreferences(PREFS, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public static Session get() {
        if (session == null) {
            session = new Session();
        }
        return session;
    }

    public void setUser(User user) {
        editor.putString(USER, new Gson().toJson(user));
        editor.apply();
    }

    public User getUser() {
        return new Gson().fromJson(sharedPreferences.getString(USER, ""), User.class);
    }

    public void setWorkInProgress(boolean workInProgress) {
        editor.putBoolean(WORK_IN_PROGRESS, workInProgress);
        editor.apply();
    }

    public boolean isWorkInProgress() {
        return sharedPreferences.getBoolean(WORK_IN_PROGRESS, false);
    }


}
