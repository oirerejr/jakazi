package com.serftre.jakazi.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.PatternMatcher;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.serftre.jakazi.Constants;
import com.serftre.jakazi.Firebase.JKFirebase;
import com.serftre.jakazi.R;
import com.serftre.jakazi.helpers.Ui;
import com.serftre.jakazi.models.User;

import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterActivity extends AppCompatActivity {

    @BindView(R.id.full_name)
    EditText fullName;

    @BindView(R.id.email_address)
    EditText emailAddress;

    @BindView(R.id.phone_number)
    EditText phoneNumber;

    @BindView(R.id.password)
    EditText password;

    @BindView(R.id.repeat_password)
    EditText confirmPassword;

    private FirebaseAuth mAuth;

    private interface RegistrationListener {
        void RegistrationComplete(FirebaseUser user);

        void RegistrationFailed(String cause);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null) {
            startActivity(new Intent(this, MaidActivity.class));
        }
//        updateUI(currentUser);
    }

    @OnClick(R.id.register)
    public void register() {
        final String name = fullName.getText().toString().trim();
        final String email = emailAddress.getText().toString().trim();
        final String phone = phoneNumber.getText().toString().trim();
        String password = this.password.getText().toString().trim();
        String confirmPassword = this.confirmPassword.getText().toString().trim();

        Toast toast = Toast.makeText(this, "", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);

        if (TextUtils.isEmpty(name)) {
            toast.setText("Please enter your full name");
            toast.show();
        } else if (TextUtils.isEmpty(email)) {
            toast.setText("Please enter your email address");
            toast.show();
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            toast.setText("Email format provided is not valid");
            toast.show();
        } else if (password.length() < 6) {
            toast.setText("Password length less than 6 characters");
            toast.show();
        } else if (!password.equals(confirmPassword)) {
            toast.setText("Passwords do not match");
            toast.show();
        } else {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Registering...");
            progressDialog.setIndeterminate(true);
            progressDialog.show();
            tryRegistration(email, password, new RegistrationListener() {
                @Override
                public void RegistrationComplete(FirebaseUser user) {
                    User u = new User();
                    u.setName(name);
                    u.setEmail(email);
                    u.setPhone(phone);
                    u.setId(user.getUid());
//                    String key = JKFirebase.get(Constants.CLIENTS).push().getKey();
                    JKFirebase.get(Constants.CLIENTS).child(user.getUid()).setValue(u);
                    progressDialog.dismiss();
                    startActivity(new Intent(RegisterActivity.this, MainActivity.class));
                    finish();
                }

                @Override
                public void RegistrationFailed(String cause) {
                    Toast.makeText(RegisterActivity.this, cause, Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            });
        }

    }

    private void tryRegistration(String email, String password, final RegistrationListener registrationListener) {

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = mAuth.getCurrentUser();
                            registrationListener.RegistrationComplete(user);
                        } else {
                            registrationListener.RegistrationFailed(task.getException().getMessage());
                        }

                    }
                });

    }

    @Override
    protected void onResume() {
        super.onResume();
        Ui.getInstance().hideSystemUI(getWindow());
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            Ui.getInstance().hideSystemUI(getWindow());
        }
    }

}
