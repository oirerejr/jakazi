package com.serftre.jakazi.activities;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.serftre.jakazi.Constants;
import com.serftre.jakazi.Firebase.JKFirebase;
import com.serftre.jakazi.R;
import com.serftre.jakazi.helpers.PhoneListener;
import com.serftre.jakazi.helpers.Session;
import com.serftre.jakazi.logs.Call;
import com.serftre.jakazi.models.Maid;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MaidActivity extends AppCompatActivity {

    public static final int RESPONSE = 101;
    public static final int CALL_PHONE_REQUEST = 56;
    private boolean pendingTransaction;
    private boolean workInProgress;

    @BindView(R.id.message)
    FloatingActionButton message;

    @BindView(R.id.release)
    Button release;

    @BindView(R.id.status)
    TextView status;

    @BindView(R.id.name)
    TextView name;

    @BindView(R.id.rating)
    TextView rating;

    @BindView(R.id.return_rate)
    TextView returnRate;

    @BindView(R.id.service_rate)
    TextView serviceRate;

    @BindView(R.id.description)
    TextView description;

    @BindView(R.id.quality_rating)
    SimpleRatingBar qualityRating;

    @BindView(R.id.punctuality_rating)
    SimpleRatingBar puncRating;

    @BindView(R.id.call)
    FloatingActionButton call;

    private Maid maid;
    private static String callLogId;

    private TelephonyManager telephonyManager;
    private PhoneListener phoneStateListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maid);
        ButterKnife.bind(this);

        telephonyManager =
                (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        phoneStateListener = new PhoneListener();

        PhoneListener.PhoneListenerInterface phoneListenerInterface = new PhoneListener.PhoneListenerInterface() {
            @Override
            public void ringing(int state, String phoneNumber) {
            }

            @Override
            public void offHook(int state, String phoneNumber) {
                pendingTransaction = true;
            }

            @Override
            public void idle(int state, String phoneNumber) {
                if (pendingTransaction) {
                    pendingTransaction = false;
                    if (!Session.get().isWorkInProgress()) {
                        Intent intent = new Intent(MaidActivity.this, AgreementActivity.class);
                        intent.putExtras(getIntent());
                        if (MaidActivity.callLogId != null) {
                            intent.putExtra("log", MaidActivity.callLogId);
                        }
                        startActivity(intent);
                    }
                }
            }
        };

        phoneStateListener.setListener(phoneListenerInterface);

        Intent intent = getIntent();

        maid = new Gson().fromJson(intent.getStringExtra("maid"), Maid.class);

        name.setText(maid.getName());
        serviceRate.setText(String.valueOf(maid.getServiceRate()));

        if (maid.isStatus()) {
            Drawable drawable = ContextCompat.getDrawable(this, R.drawable.item_maid_idle_background);
            status.setText("Available");
            status.setBackground(drawable);
        } else {
            call.setVisibility(View.GONE);
            Drawable drawable = ContextCompat.getDrawable(this, R.drawable.item_maid_busy_background);
            status.setText("Busy");
            status.setBackground(drawable);
        }

        description.setText(String.format("%s has completed %s services", maid.getName(), "50"));

        if (maid.getRating() <= 0) {
            rating.setText(String.valueOf(5.0));
            returnRate.setText("N/A");
        } else {
            rating.setText(String.valueOf(maid.getRating()));
        }

        int flag = intent.getIntExtra("flag", 0);

        if (flag == 1 || Session.get().isWorkInProgress()) {
            Drawable drawable = ContextCompat.getDrawable(this, R.drawable.item_maid_idle_background);
            call.setSize(FloatingActionButton.SIZE_MINI);
            release.setVisibility(View.VISIBLE);
            status.setText("Work in progress");
            status.setBackground(drawable);
            Session.get().setWorkInProgress(true);
        }

    }

    @OnClick(R.id.back)
    public void back() {
        finish();
    }

    @OnClick(R.id.release)
    public void release() {
        startActivity(new Intent(this, ReleaseActivity.class));
    }

    @OnClick(R.id.call)
    public void call() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            startCall();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE, Manifest.permission.READ_PHONE_STATE}, CALL_PHONE_REQUEST);
        }

    }

    private void startCall() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle("Start Call")
                .setMessage("Are you sure you want to call Edward Oirere")
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .setPositiveButton("Call", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (ContextCompat.checkSelfPermission(MaidActivity.this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED &&
                                ContextCompat.checkSelfPermission(MaidActivity.this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                            Intent intent = new Intent(Intent.ACTION_CALL);
                            intent.setData(Uri.parse(String.format("tel:%s", maid.getPhone())));

                            if (telephonyManager != null) {
                                telephonyManager.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);
                            }

                            Call call = new Call();
                            call.setCaller(Session.get().getUser().getPhone());
                            call.setReceiver(maid.getPhone());
                            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM, YYYY hh:mm:ss", Locale.getDefault());
                            call.setTime(simpleDateFormat.format(new Date()));

                            String key = JKFirebase.get(Constants.CALLS).push().getKey();
                            JKFirebase.get(Constants.CALLS).child(key).setValue(call);

                            MaidActivity.callLogId = key;

                            startActivity(intent);
                        }
                    }
                });

        builder.show();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            startCall();
        }
    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//        if(pendingTransaction) {
//            firstResume = false;
//        }
//        if(!firstResume) {
//            Log.e("Res", "Resumed");
//        }
//
//    }

}
