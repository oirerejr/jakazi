package com.serftre.jakazi.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.serftre.jakazi.R;
import com.serftre.jakazi.activities.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RatingActivity extends AppCompatActivity implements SimpleRatingBar.OnRatingBarChangeListener {

    private boolean overallRated, qualityRated, punctualityRated;

    @BindView(R.id.overall_rating)
    SimpleRatingBar overallRating;

    @BindView(R.id.punctuality_rating)
    SimpleRatingBar punctualityRating;

    @BindView(R.id.quality_rating)
    SimpleRatingBar qualityRating;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating);
        ButterKnife.bind(this);

        overallRating.setOnRatingBarChangeListener(this);
        punctualityRating.setOnRatingBarChangeListener(this);
        qualityRating.setOnRatingBarChangeListener(this);

    }

    @Override
    public void onRatingChanged(SimpleRatingBar simpleRatingBar, float rating, boolean fromUser) {

        if (fromUser) {
            switch (simpleRatingBar.getId()) {
                case R.id.overall_rating:
                    overallRated = true;
                    break;
                case R.id.punctuality_rating:
                    punctualityRated = true;
                    break;
                case R.id.quality_rating:
                    qualityRated = true;
                    break;
            }
        }

        if (overallRated && qualityRated && punctualityRated) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }

    }

}
