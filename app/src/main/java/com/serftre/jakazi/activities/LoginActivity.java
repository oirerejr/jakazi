package com.serftre.jakazi.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.redmadrobot.inputmask.MaskedTextChangedListener;
import com.serftre.jakazi.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {

    //    @BindView(R.id.email_address)
//    EditText emailAddress;

    @BindView(R.id.phone_number)
    EditText phoneNumber;

    @BindView(R.id.get_started)
    Button getStarted;

    private String phone;
    private MaskedTextChangedListener listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        listener = MaskedTextChangedListener.Companion.installOn(
                phoneNumber,
                "{+254} [000] [000] [000]",
                new MaskedTextChangedListener.ValueListener() {
                    @Override
                    public void onTextChanged(boolean maskFilled, @NonNull final String extractedValue) {
                        getStarted.setVisibility(maskFilled ? View.VISIBLE : View.GONE);
                        phone = extractedValue;
                    }
                }
        );

        phoneNumber.setHint(listener.placeholder());

//        if (Session.get().getUser() != null) {
//            emailAddress.setText(Session.get().getUser().getEmail());
//            password.requestFocus();
//        }

    }


    @OnClick(R.id.get_started)
    public void getStarted() {
        Intent intent = new Intent(this, OTPActivity.class);
        intent.putExtra("phone", phone);
        startActivity(intent);
//        startPhoneNumberVerification(phone);
    }

//    @OnClick(R.id.sign_in)
//    public void signIn() {
//        final String email = emailAddress.getText().toString().trim();
//        String password = this.password.getText().toString().trim();
//
//        Toast toast = Toast.makeText(this, "", Toast.LENGTH_SHORT);
//        toast.setGravity(Gravity.CENTER, 0, 0);
//
//        if (TextUtils.isEmpty(email)) {
//            toast.setText("Please enter your email address");
//            toast.show();
//        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
//            toast.setText("Email format provided is not valid");
//            toast.show();
//        } else {
//            final ProgressDialog progressDialog = new ProgressDialog(this);
//            progressDialog.setMessage("Registering...");
//            progressDialog.setIndeterminate(true);
//            progressDialog.show();
//
//            mAuth.signInWithEmailAndPassword(email, password)
//                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
//                        @Override
//                        public void onComplete(@NonNull Task<AuthResult> task) {
//                            if (task.isSuccessful()) {
//                                FirebaseUser user = mAuth.getCurrentUser();
//                                JKFirebase.get(Constants.CLIENTS).child(user.getUid()).addValueEventListener(new ValueEventListener() {
//                                    @Override
//                                    public void onDataChange(DataSnapshot dataSnapshot) {
//                                        User user = dataSnapshot.getValue(User.class);
//                                        Session.get().setUser(user);
//                                        progressDialog.dismiss();
//                                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
//                                        finish();
//                                    }
//
//                                    @Override
//                                    public void onCancelled(DatabaseError databaseError) {
//                                        progressDialog.dismiss();
//                                    }
//                                });
//                            } else {
//                                Toast.makeText(LoginActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
//                                progressDialog.dismiss();
//                            }
//                        }
//                    });
//
//        }
//
//    }
//
//    @OnClick(R.id.register)
//    public void register() {
//        startActivity(new Intent(this, RegisterActivity.class));
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        Ui.getInstance().hideSystemUI(getWindow());
//        if(Session.get().getUser() != null) {
//            startActivity(new Intent(this, MainActivity.class));
//        }
//    }
//
//    @Override
//    public void onWindowFocusChanged(boolean hasFocus) {
//        super.onWindowFocusChanged(hasFocus);
//        if (hasFocus) {
//            Ui.getInstance().hideSystemUI(getWindow());
//        }
//    }
}
