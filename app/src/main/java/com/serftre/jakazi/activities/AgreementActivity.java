package com.serftre.jakazi.activities;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.codemybrainsout.ratingdialog.RatingDialog;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.serftre.jakazi.Constants;
import com.serftre.jakazi.Firebase.JKFirebase;
import com.serftre.jakazi.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class AgreementActivity extends AppCompatActivity {

    private String log;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agreement);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        log = intent.getStringExtra("log");

    }

    @Override
    public void onBackPressed() {
    }

    private void recordEntry(String entry) {
        JKFirebase.get(Constants.AGREEMENTS).child(entry).child("total").runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                Integer currentValue = mutableData.getValue(Integer.class);
                if (currentValue == null) {
                    mutableData.setValue(1);
                } else {
                    mutableData.setValue(currentValue + 1);
                }

                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(
                    DatabaseError databaseError, boolean committed, DataSnapshot dataSnapshot) {
                System.out.println("Transaction completed");
            }
        });
        JKFirebase.get(Constants.AGREEMENTS).child("total").runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                Integer currentValue = mutableData.getValue(Integer.class);
                if (currentValue == null) {
                    mutableData.setValue(1);
                } else {
                    mutableData.setValue(currentValue + 1);
                }

                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(
                    DatabaseError databaseError, boolean committed, DataSnapshot dataSnapshot) {
                System.out.println("Transaction completed");
            }
        });
    }

    @OnClick(R.id.yes)
    public void yes() {
        recordEntry(Constants.A);
        Intent intent = new Intent(this, MaidActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra("flag", 1);
        intent.putExtras(getIntent());
        startActivity(intent);
    }

    @OnClick(R.id.not_go_through)
    public void notGoThrough() {
        recordEntry(Constants.D);
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setMessage("We will work to solve the situation, please try a different client")
                .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (ContextCompat.checkSelfPermission(AgreementActivity.this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                            Intent intent = new Intent(AgreementActivity.this, MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.putExtras(getIntent());
                            startActivity(intent);
                        }
                    }
                });

        builder.show();
    }

    @OnClick(R.id.not_picked)
    public void notPicked() {
        recordEntry(Constants.M);
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle("Try again")
                .setMessage("Would you like to dial again?")
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        finish();
                    }
                })
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (ContextCompat.checkSelfPermission(AgreementActivity.this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                            Intent intent = new Intent(Intent.ACTION_CALL);
                            intent.setData(Uri.parse("tel:254700460888"));
                            intent.putExtras(getIntent());
                            startActivity(intent);
                        }
                    }
                });

        builder.show();
    }

    @OnClick(R.id.no)
    public void no() {
        recordEntry(Constants.R);
        RatingDialog ratingDialog = new RatingDialog.Builder(this)
                .title("Rate out client's politeness")
                .titleTextColor(R.color.black)
                .threshold(3)
                .positiveButtonTextColor(R.color.colorRedAccent)
                .negativeButtonTextColor(R.color.colorRedAccent)
                .formTitle("Submit Feedback")
                .formHint("Tell us what went wrong")
                .formSubmitText("Submit")
                .formCancelText("Cancel")
                .ratingBarColor(R.color.colorYellowAccent)
                .onThresholdCleared(new RatingDialog.Builder.RatingThresholdClearedListener() {
                    @Override
                    public void onThresholdCleared(RatingDialog ratingDialog, float rating, boolean thresholdCleared) {

                    }
                })
                .onRatingChanged(new RatingDialog.Builder.RatingDialogListener() {
                    @Override
                    public void onRatingSelected(float rating, boolean thresholdCleared) {

                    }
                })
                .onRatingBarFormSumbit(new RatingDialog.Builder.RatingDialogFormListener() {
                    @Override
                    public void onFormSubmitted(String feedback) {
                        setResult(RESULT_CANCELED);
                        finish();
                    }
                }).build();

        ratingDialog.show();
    }

}
