package com.serftre.jakazi.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.serftre.jakazi.Constants;
import com.serftre.jakazi.Firebase.JKFirebase;
import com.serftre.jakazi.R;
import com.serftre.jakazi.helpers.Session;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class ReleaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_release);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.back)
    public void back() {
        finish();
    }

    @OnClick({R.id.complete, R.id.innap, R.id.effective, R.id.late})
    public void releaseClicked(View view) {
        switch (view.getId()) {
            case R.id.complete:
                recordRelease(Constants.COMPLETE);
                startActivity(new Intent(this, RatingActivity.class));
                finish();
                break;
            case R.id.innap:
                recordRelease(Constants.MISBEHAVED);
                break;
            case R.id.late:
                recordRelease(Constants.LATE);
                break;
            case R.id.effective:
                recordRelease(Constants.INEFFECTIVE);
                break;
        }
        Session.get().setWorkInProgress(false);
    }

    private void recordRelease(String entry) {
        JKFirebase.get(Constants.RELEASE).child(entry).child("total").runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                Integer currentValue = mutableData.getValue(Integer.class);
                if (currentValue == null) {
                    mutableData.setValue(1);
                } else {
                    mutableData.setValue(currentValue + 1);
                }

                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(
                    DatabaseError databaseError, boolean committed, DataSnapshot dataSnapshot) {
                System.out.println("Transaction completed");
            }
        });

        JKFirebase.get(Constants.RELEASE).child("total").runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                Integer currentValue = mutableData.getValue(Integer.class);
                if (currentValue == null) {
                    mutableData.setValue(1);
                } else {
                    mutableData.setValue(currentValue + 1);
                }

                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(
                    DatabaseError databaseError, boolean committed, DataSnapshot dataSnapshot) {
                System.out.println("Transaction completed");
            }
        });
    }

}
