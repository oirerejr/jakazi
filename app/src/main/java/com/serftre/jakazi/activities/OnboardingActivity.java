package com.serftre.jakazi.activities;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

import com.serftre.jakazi.R;
import com.serftre.jakazi.fragents.OnboardingFragment;
import com.serftre.jakazi.fragents.SmartFragmentStatePagerAdapter;
import com.serftre.jakazi.helpers.Ui;
import com.serftre.jakazi.models.Page;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OnboardingActivity extends AppCompatActivity {

//    @BindView(R.id.indicator)
//    PageIndicatorView pageIndicatorView;

    @BindView(R.id.viewpager)
    ViewPager viewPager;

    @BindView(R.id.next)
    Button next;

    ArrayList<OnboardingFragment> onboardingFragments;
    ArrayList<Page> pages;

    private SmartFragmentStatePagerAdapter<OnboardingFragment> smartFragmentStatePagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onboarding);
        ButterKnife.bind(this);

        onboardingFragments = new ArrayList<>();

        onboardingFragments.add(OnboardingFragment.newInstance(R.drawable.ic_drop, "Welcome to " +
                "Jakazi", "Having a clean house is important, but ensuring trust and professionalism is just as essential."));
        onboardingFragments.add(OnboardingFragment.newInstance(R.drawable.ic_drop, "Quality",
                "With experienced clients, Jakazi is endorsed with quality services provided to " +
                        "their customers"));
        onboardingFragments.add(OnboardingFragment.newInstance(R.drawable.ic_drop,
                "Live behaviour", "With thousands of house keepers at you disposal, we like to keep you updated about any behaviour change to help you decide on the house keepers you choose."));
        onboardingFragments.add(OnboardingFragment.newInstance(R.drawable.ic_drop, "Rating", "Help us improve our customers reputation after every services they " +
                "provide by rating them"));

        pages = new ArrayList<>();

        smartFragmentStatePagerAdapter = new SmartFragmentStatePagerAdapter<OnboardingFragment>(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int i) {
                return onboardingFragments.get(i);
            }

            @Override
            public int getCount() {
                return onboardingFragments.size();
            }
        };

        viewPager.setAdapter(smartFragmentStatePagerAdapter);

//        pageIndicatorView.setViewPager(viewPager);
//        pageIndicatorView.setSelected(1);

    }

    @OnClick(R.id.next)
    public void next() {

        int current = viewPager.getCurrentItem();

        if (current == viewPager.getAdapter().getCount() - 1) {
            startActivity(new Intent(this, LoginActivity.class));
            finish();
            return;
        }

        int next = current += 1;

        viewPager.setCurrentItem(next, true);

        if (viewPager.getCurrentItem() == viewPager.getAdapter().getCount() - 1) {
            OnboardingActivity.this.next.setText("Get Started");
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        Ui.getInstance().hideSystemUI(getWindow());
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            Ui.getInstance().hideSystemUI(getWindow());
        }
    }


}
