package com.serftre.jakazi.activities;

import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.ViewStub;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.serftre.jakazi.Constants;
import com.serftre.jakazi.Firebase.JKFirebase;
import com.serftre.jakazi.R;
import com.serftre.jakazi.adapters.MaidAdapter;
import com.serftre.jakazi.adapters.ServiceListAdapter;
import com.serftre.jakazi.adapters.ToppickAdapter;
import com.serftre.jakazi.helpers.DividerItemDecoration;
import com.serftre.jakazi.helpers.Session;
import com.serftre.jakazi.models.Item;
import com.serftre.jakazi.models.Maid;
import com.serftre.jakazi.models.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

//public class MainActivity extends AppCompatActivity implements  ServiceListAdapter.ServiceListInterface, MaidAdapter.MaidClickListener {
public class MainActivity extends AppCompatActivity implements MaidAdapter.MaidClickListener {

    //    @BindView(R.id.localities)
//    Spinner localitiesList;
//    @BindView(R.id.services_list)
//    RecyclerView serviceList;
//    @BindView(R.id.viewed_services)
//    TextView showingServices;
    @BindView(R.id.top_picks)
    RecyclerView topPicks;

    @BindView(R.id.greetings)
    TextView greetings;

//    @BindView(R.id.layout)
//    ViewStub layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEEE", Locale.getDefault());
        greetings.setText(String.format("Good %s,", simpleDateFormat.format(new Date())));

//        layout.inflate();

//        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this,
//                ContextCompat.getColor(this, R.color.colorGrayAccent), 1);
//
//        maidList.addItemDecoration(dividerItemDecoration);
//
//        showingServices.setText(String.format(getResources().getString(R.string.showing_residential_services), "All"));
//
//        JKFirebase.get(Constants.LOCALITIES).addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//
//                List<String> localities = new ArrayList<>();
//                for (DataSnapshot snapshot :
//                        dataSnapshot.getChildren()) {
//                    Item item = snapshot.getValue(Item.class);
//                    localities.add(item.getName());
//                }
//                ArrayAdapter<String> adapter = new ArrayAdapter<String>(MainActivity.this,
//                        R.layout.locality_text_layout, localities);
//                localitiesList.setAdapter(adapter);
//
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
//
//        JKFirebase.get(Constants.SERVICES).addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//
//                ArrayList<Item> services = new ArrayList<>();
//                for (DataSnapshot snapshot :
//                        dataSnapshot.getChildren()) {
//                    Item item = snapshot.getValue(Item.class);
//                    services.add(item);
//                }
//                ServiceListAdapter serviceListAdapter = new ServiceListAdapter(MainActivity.this, services);
//                serviceList.setAdapter(serviceListAdapter);
//
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
//
        JKFirebase.get(Constants.HOUSEKEEPERS).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                ArrayList<Maid> maids = new ArrayList<>();
                for (DataSnapshot snapshot :
                        dataSnapshot.getChildren()) {
                    Maid maid = snapshot.getValue(Maid.class);
                    maids.add(maid);
                }

                MaidAdapter maidAdapter = new MaidAdapter(MainActivity.this, maids);
                topPicks.setAdapter(maidAdapter);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    //    @Override
//    protected void onResume() {
//        super.onResume();
//        if (Session.get().isWorkInProgress()) {
//            startActivity(new Intent(this, MaidActivity.class));
//            finish();
//        }
//    }
//
//    @Override
//    public void serviceClicked(Item service) {
//        showingServices.setText(String.format(getResources().getString(R.string.showing_residential_services), service.getName()));
//    }
//
    @Override
    public void maidClicked(Maid maid) {
        Intent intent = new Intent(this, MaidActivity.class);
        intent.putExtra("maid", new Gson().toJson(maid));
        startActivity(intent);
    }
}
