package com.serftre.jakazi.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.serftre.jakazi.helpers.Ui;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startActivity(new Intent(this, OnboardingActivity.class));
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Ui.getInstance().hideSystemUI(getWindow());
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if(hasFocus) {
            Ui.getInstance().hideSystemUI(getWindow());
        }
    }
}
