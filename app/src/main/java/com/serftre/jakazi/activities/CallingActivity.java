package com.serftre.jakazi.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.serftre.jakazi.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class CallingActivity extends AppCompatActivity {

    public static final int RESPONSE = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calling);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.hang_up)
    public void hangup() {
        startActivity(new Intent(this, AgreementActivity.class));
        finish();
    }

    @Override
    public void onBackPressed() {
    }

}
