package com.serftre.jakazi.Firebase;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by oirere on 04/12/2018.
 */

public class JKFirebase {

    private static final FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
    private static DatabaseReference databaseReference;
    private static final JKFirebase jkFirebase = new JKFirebase();

    public JKFirebase() {
        databaseReference = firebaseDatabase.getReference();
    }

    public static JKFirebase get() {
        return jkFirebase;
    }

    public static DatabaseReference get(String reference) {
        if (databaseReference == null) {
            databaseReference = firebaseDatabase.getReference();
        }
        return databaseReference.child(reference);
    }

}
