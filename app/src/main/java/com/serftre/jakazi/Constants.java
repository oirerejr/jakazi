package com.serftre.jakazi;

/**
 * Created by oirere on 04/12/2018.
 */

public class Constants {

    public static final String CLIENTS = "clients";
    public static final String SERVICES = "services";
    public static final String HOUSEKEEPERS = "housekeepers";
    public static final String CALLS = "calls";
    public static final String LOCALITIES = "localities";

    public static final String AGREEMENTS = "agreements";
    public static final String RELEASE = "release";
    public static final String A = "A";
    public static final String R = "R";
    public static final String M = "M";
    public static final String D = "D";

    public static final String LATE = "late";
    public static final String COMPLETE = "complete";
    public static final String MISBEHAVED = "misbehaved";
    public static final String INEFFECTIVE = "ineffective";

}
